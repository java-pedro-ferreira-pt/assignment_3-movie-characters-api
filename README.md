# OpenAPI Definition v0 Documentation
## Introduction:
This document provides a comprehensive guide to the OpenAPI definition v0 of a RESTful API. The API is written in accordance with OpenAPI Specification (OAS) 3 and is accessible at the endpoint /v3/api-docs. 

The API is hosted on a generated server URL http://localhost:8080 and has three controllers, movie-controller, franchise-controller, and character-controller.
## Controllers:
### Movie-controller: 
This controller allows you to perform CRUD (Create, Read, Update, Delete) operations on movie resources.
- GET /api/movies/{id}: Retrieves a movie resource with the given id.
- PUT /api/movies/{id}: Updates a movie resource with the given id.
- DELETE /api/movies/{id}: Deletes a movie resource with the given id.
- GET /api/movies: Retrieves a list of all movie resources.
- POST /api/movies: Creates a new movie resource.
### Franchise-controller: 
This controller allows you to perform CRUD operations on franchise resources.
- GET /api/franchises/{id}: Retrieves a franchise resource with the given id.
- PUT /api/franchises/{id}: Updates a franchise resource with the given id.
- DELETE /api/franchises/{id}: Deletes a franchise resource with the given id.
- GET /api/franchises: Retrieves a list of all franchise resources.
- POST /api/franchises: Creates a new franchise resource.
### Character-controller: This controller allows you to perform CRUD operations on character resources.
- GET /api/characters/{id}: Retrieves a character resource with the given id.
- PUT /api/characters/{id}: Updates a character resource with the given id.
- DELETE /api/characters/{id}: Deletes a character resource with the given id.
- GET /api/characters: Retrieves a list of all character resources.
- POST /api/characters: Creates a new character resource.
## Schemas:
The API has three resource schemas, Movie, Franchise, and Character, and three corresponding data transfer object (DTO) schemas, MovieDTO, FranchiseDTO, and CharacterDTO. These schemas define the structure of the data used in the API.
## Conclusion:
This documentation provides a detailed overview of the OpenAPI definition v0 of a RESTful API. It covers the controllers, endpoints, and schemas used in the API. Use this document as a reference for accessing and utilizing the API effectively.