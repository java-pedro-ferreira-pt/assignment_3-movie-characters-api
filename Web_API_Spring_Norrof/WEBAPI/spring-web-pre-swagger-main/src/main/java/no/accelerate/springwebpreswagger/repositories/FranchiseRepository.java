package no.accelerate.springwebpreswagger.repositories;
import no.accelerate.springwebpreswagger.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Set;

public interface FranchiseRepository extends JpaRepository<Franchise, java.lang.Integer> {
    @Query("SELECT f FROM Franchise f WHERE f.franchise_name = :franchiseName")
    Set<Franchise> findAllByName(@Param("franchiseName") String franchiseName);
}
