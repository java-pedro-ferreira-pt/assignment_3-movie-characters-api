package no.accelerate.springwebpreswagger.services.franchise;
import no.accelerate.springwebpreswagger.exceptions.FranchiseNotFoundException;
import no.accelerate.springwebpreswagger.mappers.FranchiseMapper;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.dto.character.CharacterDTO;
import no.accelerate.springwebpreswagger.models.dto.franchise.FranchiseDTO;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import no.accelerate.springwebpreswagger.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    private final FranchiseRepository franchiseRepository;
    private final FranchiseMapper franchiseMapper;
    @Autowired
    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, FranchiseMapper franchiseMapper) {
        this.franchiseRepository = franchiseRepository;
        this.franchiseMapper = franchiseMapper;
    }
    @Override
    public Collection<FranchiseDTO> getAllFranchises() {
        Collection<Franchise> franchises = franchiseRepository.findAll();
        return franchiseMapper.franchiseToFranchiseDTO(franchises);
    }
    @Override
    public FranchiseDTO getFranchiseById(Integer id) throws FranchiseNotFoundException {
        Franchise franchise = findById(id);
        return franchiseMapper.franchiseToFranchiseDTO(franchise);
    }
    @Override
    public FranchiseDTO addFranchise(Franchise franchise) {
        Franchise savedFranchise = add(franchise);
        return franchiseMapper.franchiseToFranchiseDTO(savedFranchise);
    }
    @Override
    public FranchiseDTO updateFranchise(java.lang.Integer id, Franchise franchise) throws FranchiseNotFoundException {
        try {
            Franchise foundFranchise = findById(id);
            franchise.setFranchise_id(id);
            Franchise updatedFranchise = update(franchise);
            return franchiseMapper.franchiseToFranchiseDTO(updatedFranchise);
        } catch (FranchiseNotFoundException e) {
            throw new FranchiseNotFoundException(id);
        }
    }
    @Override
    public void deleteFranchise (java.lang.Integer id) {
        deleteById(id);
    }
    @Override
    public Franchise findById(java.lang.Integer id) throws FranchiseNotFoundException {
            return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }
    @Override
    @Transactional
    public void deleteById(Integer id) {
            // Set relationships to null so we can delete without referential problems
            Franchise franchise = franchiseRepository.findById(id).get();
            franchise.getMovies().forEach(s -> s.setFranchise_id(null));
            franchiseRepository.delete(franchise);
    }
    @Override
    public boolean exists(java.lang.Integer id) {
        try {
            return franchiseRepository.existsById(id);
        } catch (Exception e) {
            return false;
        }
    }
}