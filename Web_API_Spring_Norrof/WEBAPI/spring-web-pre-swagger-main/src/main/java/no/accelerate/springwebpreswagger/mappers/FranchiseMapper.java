package no.accelerate.springwebpreswagger.mappers;

import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.dto.franchise.FranchiseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
@Component
@Mapper(componentModel = "spring")
public interface FranchiseMapper {

    @Mapping(target = "franchise_id", source = "franchise.franchise_id")
    @Mapping(target = "franchise_name", source = "franchise.franchise_name")
    @Mapping(target = "description", source = "franchise.description")
    @Mapping(target = "movies", source = "franchise.movies")
    FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises);

    @Named(value = "moviesToMoviesId")
    default Set<Integer> map(Set<Franchise> value) {
        if(value == null)
            return null;
        return value.stream()
                .map(s -> s.getFranchise_id())
                .collect(Collectors.toSet());
    }
}