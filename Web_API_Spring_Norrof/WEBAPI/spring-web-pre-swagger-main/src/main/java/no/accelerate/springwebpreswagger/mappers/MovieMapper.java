package no.accelerate.springwebpreswagger.mappers;

import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Mapper(componentModel = "spring")
public interface MovieMapper {

    @Mapping(target = "movie_id", source = "movie.movie_id")
    @Mapping(target = "title", source = "movie.title")
    @Mapping(target = "genre", source = "movie.genre")
    @Mapping(target = "release_year", source = "movie.release_year")
    @Mapping(target = "director", source = "movie.director")
    @Mapping(target = "picture", source = "movie.picture")
    @Mapping(target = "trailer", source = "movie.trailer")
    @Mapping(target = "franchise_id", source = "movie.franchise_id")
    @Mapping(target = "characters", source = "movie.characters")
    MovieDTO movieToMovieDTO(Movie movie);
    Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    @Named(value = "charactersToCharactersId")
    default Set<Integer> map(Set<Movie> value) {
        if(value == null)
            return null;
        return value.stream()
                .map(s -> s.getMovie_id())
                .collect(Collectors.toSet());
    }
}