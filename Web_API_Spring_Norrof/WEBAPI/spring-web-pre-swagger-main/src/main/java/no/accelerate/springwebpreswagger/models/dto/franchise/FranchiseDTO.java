package no.accelerate.springwebpreswagger.models.dto.franchise;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.springwebpreswagger.models.Character;
import no.accelerate.springwebpreswagger.models.Movie;
//import no.accelerate.springwebpreswagger.models.enumerators.Gender;

import java.util.Set;

@Getter
@Setter
public class FranchiseDTO {
    private Integer franchise_id;
    private String franchise_name;
    private String description;
    private Set<Movie> movies;
}