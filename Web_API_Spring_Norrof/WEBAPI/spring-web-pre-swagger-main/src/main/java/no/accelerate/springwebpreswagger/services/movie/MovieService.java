package no.accelerate.springwebpreswagger.services.movie;
import no.accelerate.springwebpreswagger.exceptions.MovieNotFoundException;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import no.accelerate.springwebpreswagger.services.CrudService;
import java.util.Collection;

public interface MovieService extends CrudService<Movie, Integer> {
    Collection<MovieDTO> getAllMovies();
    MovieDTO getMovieById(Integer id) throws MovieNotFoundException;
    MovieDTO addMovie(Movie movie);
    MovieDTO updateMovie(Integer id, Movie movie) throws MovieNotFoundException;
    void deleteMovie(Integer id);
}
