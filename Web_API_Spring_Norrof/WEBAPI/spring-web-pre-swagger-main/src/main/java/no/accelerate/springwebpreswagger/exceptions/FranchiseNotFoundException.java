package no.accelerate.springwebpreswagger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchiseNotFoundException extends Exception {
    public FranchiseNotFoundException(int id) {
        super(String.format("Franchise with id %d not found", id));
    }
}