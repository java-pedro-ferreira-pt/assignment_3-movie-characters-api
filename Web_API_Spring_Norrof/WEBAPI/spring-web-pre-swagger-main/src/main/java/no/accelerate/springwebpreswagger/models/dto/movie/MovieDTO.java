package no.accelerate.springwebpreswagger.models.dto.movie;

import lombok.Getter;
import lombok.Setter;
import no.accelerate.springwebpreswagger.models.Character;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.dto.character.CharacterDTO;
//import no.accelerate.springwebpreswagger.models.enumerators.Gender;

import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private Integer movie_id;
    private String title;
    private String genre;
    private String release_year;
    private String director;
    private String picture;
    private String trailer;
    private Franchise franchise_id;
    private Set<Character> characters;
}