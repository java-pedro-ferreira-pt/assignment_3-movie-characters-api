package no.accelerate.springwebpreswagger.services.franchise;
import no.accelerate.springwebpreswagger.exceptions.FranchiseNotFoundException;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.dto.character.CharacterDTO;
import no.accelerate.springwebpreswagger.models.dto.franchise.FranchiseDTO;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import no.accelerate.springwebpreswagger.services.CrudService;
import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Collection<FranchiseDTO> getAllFranchises();
    FranchiseDTO getFranchiseById(Integer id) throws FranchiseNotFoundException;
    FranchiseDTO addFranchise(Franchise franchise);
    FranchiseDTO updateFranchise(Integer id, Franchise franchise) throws FranchiseNotFoundException;
    void deleteFranchise(Integer id);
}
