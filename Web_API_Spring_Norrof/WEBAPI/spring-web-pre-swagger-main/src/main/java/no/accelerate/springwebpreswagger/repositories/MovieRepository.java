package no.accelerate.springwebpreswagger.repositories;
import no.accelerate.springwebpreswagger.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Set;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query("SELECT m FROM Movie m WHERE m.movie_id = :movieName")
    Set<Movie> findAllByName(@Param("movieName") String movieName);
}
