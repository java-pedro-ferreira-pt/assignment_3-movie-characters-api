package no.accelerate.springwebpreswagger.services.movie;
import no.accelerate.springwebpreswagger.exceptions.CharacterNotFoundException;
import no.accelerate.springwebpreswagger.exceptions.MovieNotFoundException;
import no.accelerate.springwebpreswagger.mappers.MovieMapper;
import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import no.accelerate.springwebpreswagger.repositories.CharacterRepository;
import no.accelerate.springwebpreswagger.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final MovieMapper movieMapper;
    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository, MovieMapper movieMapper) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
    }
    @Override
    public Collection<MovieDTO> getAllMovies() {
        Collection<Movie> movies = movieRepository.findAll();
        return movieMapper.movieToMovieDTO(movies);
    }
    @Override
    public MovieDTO getMovieById(Integer id) throws MovieNotFoundException {
        Movie movie = findById(id);
        return movieMapper.movieToMovieDTO(movie);
    }
    @Override
    public MovieDTO addMovie(Movie movie) {
        Movie savedMovie = add(movie);
        return movieMapper.movieToMovieDTO(savedMovie);
    }
    @Override
    public MovieDTO updateMovie(Integer id, Movie movie) throws MovieNotFoundException {
        try {
            Movie foundMovie = findById(id);
            movie.setMovie_id(id);
            Movie updatedMovie = update(movie);
            return movieMapper.movieToMovieDTO(updatedMovie);
        } catch (MovieNotFoundException e) {
            throw new MovieNotFoundException(id);
        }
    }
    @Override
    public void deleteMovie (Integer id) {
        deleteById(id);
    }
    @Override
    public Movie findById(Integer id) throws MovieNotFoundException {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }
    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }
    @Override
    public boolean exists(Integer id) {
        try {
            return movieRepository.existsById(id);
        } catch (Exception e) {
            return false;
        }
    }
}