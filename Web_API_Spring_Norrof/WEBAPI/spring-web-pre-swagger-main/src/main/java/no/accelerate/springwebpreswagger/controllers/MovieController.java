package no.accelerate.springwebpreswagger.controllers;
import no.accelerate.springwebpreswagger.exceptions.MovieNotFoundException;
import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import no.accelerate.springwebpreswagger.services.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;


@RestController
@RequestMapping("/api/movies")
public class MovieController {
    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }
    @GetMapping
    public Collection<MovieDTO> getAllMovies() {
        return movieService.getAllMovies();
    }
    @GetMapping("/{id}")
    public MovieDTO getMovieById(@PathVariable("id") Integer id) throws MovieNotFoundException {
        return movieService.getMovieById(id);
    }
    @PostMapping
    public MovieDTO addMovie(@RequestBody Movie movie) {
        return movieService.addMovie(movie);
    }
    @PutMapping("/{id}")
    public MovieDTO updateMovie(@PathVariable("id") Integer id, @RequestBody Movie movie) throws MovieNotFoundException {
        return movieService.updateMovie(id, movie);
    }
    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable("id") Integer id) {
        movieService.deleteMovie(id);
    }
}