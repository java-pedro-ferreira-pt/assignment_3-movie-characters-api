package no.accelerate.springwebpreswagger.controllers;
import no.accelerate.springwebpreswagger.exceptions.FranchiseNotFoundException;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.dto.franchise.FranchiseDTO;
import no.accelerate.springwebpreswagger.services.franchise.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

@RestController
@RequestMapping("/api/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;

    @Autowired
    public FranchiseController(FranchiseService franchiseService) {
        this.franchiseService = franchiseService;
    }
    @GetMapping
    public Collection<FranchiseDTO> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }
    @GetMapping("/{id}")
    public FranchiseDTO getFranchiseById(@PathVariable("id") Integer id) throws FranchiseNotFoundException {
        return franchiseService.getFranchiseById(id);
    }
    @PostMapping
    public FranchiseDTO addFranchise(@RequestBody Franchise franchise) {
        return franchiseService.addFranchise(franchise);
    }
    @PutMapping("/{id}")
    public FranchiseDTO updateFranchise(@PathVariable("id") java.lang.Integer id, @RequestBody Franchise franchise) throws FranchiseNotFoundException {
        return franchiseService.updateFranchise(id, franchise);
    }
    @DeleteMapping("/{id}")
    public void deleteFranchise(@PathVariable("id") java.lang.Integer id) {
        franchiseService.deleteFranchise(id);
    }
}