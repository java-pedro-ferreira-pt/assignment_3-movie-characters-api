package no.accelerate.springwebpreswagger.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.annotation.processing.Generated;
import no.accelerate.springwebpreswagger.models.Franchise;
import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.franchise.FranchiseDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T22:03:24+0000",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 19.0.1 (Oracle Corporation)"
)
@Component
public class FranchiseMapperImpl implements FranchiseMapper {

    @Override
    public FranchiseDTO franchiseToFranchiseDTO(Franchise franchise) {
        if ( franchise == null ) {
            return null;
        }

        FranchiseDTO franchiseDTO = new FranchiseDTO();

        franchiseDTO.setFranchise_id( franchise.getFranchise_id() );
        franchiseDTO.setFranchise_name( franchise.getFranchise_name() );
        franchiseDTO.setDescription( franchise.getDescription() );
        Set<Movie> set = franchise.getMovies();
        if ( set != null ) {
            franchiseDTO.setMovies( new LinkedHashSet<Movie>( set ) );
        }

        return franchiseDTO;
    }

    @Override
    public Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises) {
        if ( franchises == null ) {
            return null;
        }

        Collection<FranchiseDTO> collection = new ArrayList<FranchiseDTO>( franchises.size() );
        for ( Franchise franchise : franchises ) {
            collection.add( franchiseToFranchiseDTO( franchise ) );
        }

        return collection;
    }
}
