package no.accelerate.springwebpreswagger.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.annotation.processing.Generated;
import no.accelerate.springwebpreswagger.models.Character;
import no.accelerate.springwebpreswagger.models.Movie;
import no.accelerate.springwebpreswagger.models.dto.movie.MovieDTO;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T22:03:24+0000",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 19.0.1 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl implements MovieMapper {

    @Override
    public MovieDTO movieToMovieDTO(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setMovie_id( movie.getMovie_id() );
        movieDTO.setTitle( movie.getTitle() );
        movieDTO.setGenre( movie.getGenre() );
        movieDTO.setRelease_year( movie.getRelease_year() );
        movieDTO.setDirector( movie.getDirector() );
        movieDTO.setPicture( movie.getPicture() );
        movieDTO.setTrailer( movie.getTrailer() );
        movieDTO.setFranchise_id( movie.getFranchise_id() );
        Set<Character> set = movie.getCharacters();
        if ( set != null ) {
            movieDTO.setCharacters( new LinkedHashSet<Character>( set ) );
        }

        return movieDTO;
    }

    @Override
    public Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies) {
        if ( movies == null ) {
            return null;
        }

        Collection<MovieDTO> collection = new ArrayList<MovieDTO>( movies.size() );
        for ( Movie movie : movies ) {
            collection.add( movieToMovieDTO( movie ) );
        }

        return collection;
    }
}
